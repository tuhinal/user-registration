package com.sweetitech.userregistration.mapper;

import com.sweetitech.userregistration.entity.User;
import com.sweetitech.userregistration.entity.dto.Userdto;
import org.springframework.stereotype.Service;

@Service
public class UserReposnseMapper {

    public Userdto userResMap(User user){
        Userdto dto = new Userdto();
        dto.setId(user.getId());
        dto.setFirstName(user.getFirstName());
        dto.setLastName(user.getLastName());
        dto.setEmail(user.getEmail());
        dto.setGender(user.getGender());
        dto.setMaritalStatus(user.getMaritalStatus());



        return dto;
    }
}
