package com.sweetitech.userregistration.entity;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "address")
public class Address extends BaseEntity {
    @NotNull
    private String streetNo;

    @NotNull
    private String postOffice;

    @NotNull
    private String subDristict;

    @NotNull
    private String district;

    public Address() {
    }

    public String getStreetNo() {
        return streetNo;
    }

    public void setStreetNo(String streetNo) {
        this.streetNo = streetNo;
    }

    public String getPostOffice() {
        return postOffice;
    }

    public void setPostOffice(String postOffice) {
        this.postOffice = postOffice;
    }

    public String getSubDristict() {
        return subDristict;
    }

    public void setSubDristict(String subDristict) {
        this.subDristict = subDristict;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }


    @Override
    public String toString() {
        return "Address{" +
                "streetNo='" + streetNo + '\'' +
                ", postOffice='" + postOffice + '\'' +
                ", subDristict='" + subDristict + '\'' +
                ", district='" + district + '\'' +
                '}';
    }
}
