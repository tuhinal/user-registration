package com.sweetitech.userregistration.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "account")
public class AccountInfo extends BaseEntity{

    @NotNull
    private String bankName;

    @Column(unique = true)
    private String acountNo;

    public AccountInfo() {
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getAcountNo() {
        return acountNo;
    }

    public void setAcountNo(String acountNo) {
        this.acountNo = acountNo;
    }

    @Override
    public String toString() {
        return "AccountInfo{" +
                "bankName='" + bankName + '\'' +
                ", acountNo='" + acountNo + '\'' +
                '}';
    }
}
