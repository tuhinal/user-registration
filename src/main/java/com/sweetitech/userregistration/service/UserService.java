package com.sweetitech.userregistration.service;


import com.sweetitech.userregistration.entity.User;
import com.sweetitech.userregistration.entity.dto.Userdto;
import org.springframework.stereotype.Service;

@Service
public interface UserService {

    Userdto saveUser(Userdto userdto);

}
