package com.sweetitech.userregistration.controller;


import com.sweetitech.userregistration.entity.User;
import com.sweetitech.userregistration.entity.dto.Userdto;
import com.sweetitech.userregistration.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value="/user")
public class UserController {
    @Autowired
    private UserService userService;


    @PostMapping("/save")
    Userdto saveUser(@RequestBody Userdto userdto){

        System.out.println("***** Controller**");
        return userService.saveUser(userdto);
    }

}
