package com.sweetitech.userregistration.entity.dto;

import com.sweetitech.userregistration.entity.AccountInfo;
import com.sweetitech.userregistration.entity.NomineeInfo;

import java.util.List;

public class Userdto {

    private Long id;
    private String firstName;
    private String lastName;
    private String email;
    private String gender;
    private String maritalStatus;
    private String phone;
    private String nominee;
    private String address;


    private List<AccountInfo> accountInfos;

    private List<NomineeInfo> nomineeInfos;

    public List<NomineeInfo> getNomineeInfos() {
        return nomineeInfos;
    }

    public void setNomineeInfos(List<NomineeInfo> nomineeInfos) {
        this.nomineeInfos = nomineeInfos;
    }

    public List<AccountInfo> getAccountInfos() {
        return accountInfos;
    }

    public void setAccountInfos(List<AccountInfo> accountInfos) {
        this.accountInfos = accountInfos;
    }



    public Userdto(){

    };

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getNominee() {
        return nominee;
    }

    public void setNominee(String nominee) {
        this.nominee = nominee;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
