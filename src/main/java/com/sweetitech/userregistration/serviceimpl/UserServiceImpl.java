package com.sweetitech.userregistration.serviceimpl;

import com.sweetitech.userregistration.entity.User;
import com.sweetitech.userregistration.entity.dto.Userdto;
import com.sweetitech.userregistration.mapper.UserReposnseMapper;
import com.sweetitech.userregistration.mapper.UserRequestMapper;
import com.sweetitech.userregistration.repository.UserRepository;
import com.sweetitech.userregistration.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {


    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserRequestMapper userRequestMapper;

    @Autowired
    private UserReposnseMapper userReposnseMapper;

    @Override
    public Userdto saveUser(Userdto userdto) {
        User user = userRepository.save(userRequestMapper.userMap(userdto));
        return userReposnseMapper.userResMap(user);
    }



}
