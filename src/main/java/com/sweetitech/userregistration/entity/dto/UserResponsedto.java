package com.sweetitech.userregistration.entity.dto;

import com.sweetitech.userregistration.entity.AccountInfo;
import com.sweetitech.userregistration.entity.NomineeInfo;

import java.util.List;

public class UserResponsedto {


    private Long id;
    private String firstName;
    private String lastName;
    private String email;
    private String gender;
    private String maritalStatus;
    private  String bankName;
    private String accountNo;

   private List<AccountInfo> accountInfo;
   private List<NomineeInfo> nomineeInfos;

    public List<AccountInfo> getAccountInfo() {
        return accountInfo;
    }

    public void setAccountInfo(List<AccountInfo> accountInfo) {
        this.accountInfo = accountInfo;
    }

    public List<NomineeInfo> getNomineeInfos() {
        return nomineeInfos;
    }

    public void setNomineeInfos(List<NomineeInfo> nomineeInfos) {
        this.nomineeInfos = nomineeInfos;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }
}
