package com.sweetitech.userregistration.entity;

import javax.persistence.*;
import java.io.Serializable;
@MappedSuperclass
public class BaseEntity implements Serializable {

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
}
