package com.sweetitech.userregistration.entity;

import org.hibernate.engine.internal.Cascade;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "user")
public class User {

    @Id
    @Column(name = "id",nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;


    private String firstName;
    private String lastName;
    private String email;
    private String gender;
    private String maritalStatus;
    private String password;
    //added new
    private String phoneNo;

    //hibenrnate mapping

    @OneToOne(cascade = CascadeType.ALL)
    private Address address;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "nominee_id")
    private List<NomineeInfo> nomineeInfo;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "accountinfo_id")
    private List<AccountInfo> accountInfo;


    public void  setNomineeInfo( List<NomineeInfo> nomineeInfo){
        this.nomineeInfo = nomineeInfo;
    }

    public List<NomineeInfo> getNomineeInfo(){
        return nomineeInfo;
    }


    public User() {

    }


    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }




    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }



    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public List<AccountInfo> getAccountInfo() {
        return accountInfo;
    }

    public void setAccountInfo(List<AccountInfo> accountInfo) {
        this.accountInfo = accountInfo;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", gender='" + gender + '\'' +
                ", maritalStatus='" + maritalStatus + '\'' +
                ", password='" + password + '\'' +
                ", phoneNo='" + phoneNo + '\'' +
                ", address=" + address +
                ", nomineeInfo=" + nomineeInfo +
                ", accountInfo=" + accountInfo +
                '}';
    }
}
