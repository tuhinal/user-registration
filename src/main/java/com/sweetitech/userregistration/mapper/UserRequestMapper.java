package com.sweetitech.userregistration.mapper;

import com.sweetitech.userregistration.entity.AccountInfo;
import com.sweetitech.userregistration.entity.User;
import com.sweetitech.userregistration.entity.dto.Userdto;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserRequestMapper {

    public User userMap(Userdto userReqdto){


        User user = new User();

        user.setFirstName(userReqdto.getFirstName());
        user.setLastName(userReqdto.getLastName());
        user.setEmail(userReqdto.getEmail());
        user.setGender(userReqdto.getGender());
        user.setMaritalStatus(userReqdto.getMaritalStatus());
        user.setPhoneNo(userReqdto.getPhone());
       // user.setAccountInfo(userReqdto);
        List<AccountInfo> accountInfos = new ArrayList<>();
        user.setAccountInfo(userReqdto.getAccountInfos());
        user.setNomineeInfo(userReqdto.getNomineeInfos());



        return user;
    }


}
