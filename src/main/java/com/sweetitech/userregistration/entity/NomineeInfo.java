package com.sweetitech.userregistration.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "nominee")
public class NomineeInfo extends BaseEntity {

    @NotNull
    private String name;

    @NotNull
    @Column(unique = true)
    @Size(min = 11,max = 11,message = "Phone Number should be 11 digit")
    private String phoneNumber;

    @NotNull
    private String relation;


    public NomineeInfo() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getRelation() {
        return relation;
    }

    public void setRelation(String relation) {
        this.relation = relation;
    }

    @Override
    public String toString() {
        return "NomineeInfo{" +
                "name='" + name + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", relation='" + relation + '\'' +
                '}';
    }
}
